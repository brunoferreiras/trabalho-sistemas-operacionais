/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocacaodememoria;

import java.util.Random;

/**
 *
 * @author gdeste
 */
public class GeradorAleatorio {
    private int inicio;
    private int fim;
    
    public GeradorAleatorio(){
        this.inicio = 0;
        this.fim = 0;
    }
    public GeradorAleatorio(int inicio, int fim){
        this.inicio = inicio;
        this.fim = fim;
    }
    
    public int gerarNumero(int inicio, int fim){
        Random aux = new Random();
        return aux.nextInt(fim - inicio + 1) + inicio;
    }
}
