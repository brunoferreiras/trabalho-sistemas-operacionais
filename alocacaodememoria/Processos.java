package alocacaodememoria;

/**
 *
 * @author Bruno Ferreira
 */
public class Processos {
    private final int id;
    private final int areaOcupada;
    private final int tempoDeCriacao; //Tempo em que o processo será criado
    private final int tempoDeDuracao; //Tempo de duração do processo na memória
    private long tempoInicioExecucao; //Tempo de ínicio da alocação
    private long tempoFinalExecucao; //Tempo de fim da desalocação do processo
    private int endInicial;
    private int endFinal;
    private boolean foiAlocado;
    private boolean foiDesalocado;
    private int contPossibilidade;
    private boolean conta;
    private boolean contaAlocado;
    private boolean contaDesalocado;
    private boolean criado;
    private int tempoEspera;
    
    public Processos(int id, int areaOcupada, int tempoDeCriacao, int tempoDeDuracao){
        this.id = id;
        this.areaOcupada = areaOcupada;
        this.tempoDeCriacao = tempoDeCriacao;
        this.tempoDeDuracao = tempoDeDuracao;
        this.foiAlocado = false;
        this.foiDesalocado = false;
        this.conta = false;
        this.contPossibilidade = -1;
        this.contaAlocado = false;
        this.contaDesalocado = false;
        this.criado = false;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the areaOcupada
     */
    public int getAreaOcupada() {
        return areaOcupada;
    }

    /**
     * @return the tempoDeCriacao
     */
    public int getTempoDeCriacao() {
        return tempoDeCriacao;
    }

    /**
     * @return the tempoDeDuracao
     */
    public int getTempoDeDuracao() {
        return tempoDeDuracao;
    }

    /**
     * @return the tempoInicioExecucao
     */
    public long getTempoInicioExecucao() {
        return tempoInicioExecucao;
    }

    /**
     * @param tempoInicioExecucao the tempoInicioExecucao to set
     */
    public void setTempoInicioExecucao(long tempoInicioExecucao) {
        this.tempoInicioExecucao = tempoInicioExecucao;
    }

    /**
     * @return the tempoFinalExecucao
     */
    public long getTempoFinalExecucao() {
        return tempoFinalExecucao;
    }

    /**
     * @param tempoFinalExecucao the tempoFinalExecucao to set
     */
    public void setTempoFinalExecucao(long tempoFinalExecucao) {
        this.tempoFinalExecucao = tempoFinalExecucao;
    }    

    /**
     * @return the endInicial
     */
    public int getEndInicial() {
        return endInicial;
    }

    /**
     * @param endInicial the endInicial to set
     */
    public void setEndInicial(int endInicial) {
        this.endInicial = endInicial;
    }

    /**
     * @return the endFinal
     */
    public int getEndFinal() {
        return endFinal;
    }

    /**
     * @param endFinal the endFinal to set
     */
    public void setEndFinal(int endFinal) {
        this.endFinal = endFinal;
    }

    /**
     * @return the foiAlocado
     */
    public boolean isFoiAlocado() {
        return foiAlocado;
    }

    /**
     * @param foiAlocado the foiAlocado to set
     */
    public void setFoiAlocado(boolean foiAlocado) {
        this.foiAlocado = foiAlocado;
    }

    /**
     * @return the foiDesalocado
     */
    public boolean isFoiDesalocado() {
        return foiDesalocado;
    }

    /**
     * @param foiDesalocado the foiDesalocado to set
     */
    public void setFoiDesalocado(boolean foiDesalocado) {
        this.foiDesalocado = foiDesalocado;
    }

    /**
     * @return the contPossibilidade
     */
    public int getContPossibilidade() {
        return contPossibilidade;
    }

    /**
     * @param contPossibilidade the contPossibilidade to set
     */
    public void setContPossibilidade(int contPossibilidade) {
        this.contPossibilidade = contPossibilidade;
    }

    /**
     * @return the conta
     */
    public boolean isConta() {
        return conta;
    }

    /**
     * @param conta the conta to set
     */
    public void setConta(boolean conta) {
        this.conta = conta;
    }

    /**
     * @return the contaAlocado
     */
    public boolean isContaAlocado() {
        return contaAlocado;
    }

    /**
     * @param contaAlocado the contaAlocado to set
     */
    public void setContaAlocado(boolean contaAlocado) {
        this.contaAlocado = contaAlocado;
    }

    /**
     * @return the contaDesalocado
     */
    public boolean isContaDesalocado() {
        return contaDesalocado;
    }

    /**
     * @param contaDesalocado the contaDesalocado to set
     */
    public void setContaDesalocado(boolean contaDesalocado) {
        this.contaDesalocado = contaDesalocado;
    }

    /**
     * @return the criado
     */
    public boolean isCriado() {
        return criado;
    }

    /**
     * @param criado the criado to set
     */
    public void setCriado(boolean criado) {
        this.criado = criado;
    }

    /**
     * @return the tempoEspera
     */
    public int getTempoEspera() {
        return tempoEspera;
    }

    /**
     * @param tempoEspera the tempoEspera to set
     */
    public void setTempoEspera(int tempoEspera) {
        this.tempoEspera = tempoEspera;
    }
}