/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alocacaodememoria;

/**
 *
 * @author gdeste
 */
public class SegmentoVisual {
    private String nome;
    private int espaco;
    private int enderecoInicial;
    private int enderecoFinal;
    
    public SegmentoVisual(){
        this.espaco = 0;
        this.enderecoInicial = 0;
        this.enderecoFinal = 0;        
    }
    
    public SegmentoVisual(String nome, int espaco, int enderecoInicial, int enderecoFinal){
        this.nome = nome;
        this.espaco = espaco;
        this.enderecoInicial = enderecoInicial;
        this.enderecoFinal = enderecoFinal;        
    }
    /**
     * @return the espaco
     */
    public int getEspaco() {
        return espaco;
    }

    /**
     * @param espaco the espaco to set
     */
    public void setEspaco(int espaco) {
        this.espaco = espaco;
    }

    /**
     * @return the enderecoInicial
     */
    public int getEnderecoInicial() {
        return enderecoInicial;
    }

    /**
     * @param enderecoInicial the enderecoInicial to set
     */
    public void setEnderecoInicial(int enderecoInicial) {
        this.enderecoInicial = enderecoInicial;
    }

    /**
     * @return the enderecoFinal
     */
    public int getEnderecoFinal() {
        return enderecoFinal;
    }

    /**
     * @param enderecoFinal the enderecoFinal to set
     */
    public void setEnderecoFinal(int enderecoFinal) {
        this.enderecoFinal = enderecoFinal;
    }
    
    @Override
    public String toString(){
        return "Espaço: " + espaco + " Endereço Inicial: " + enderecoInicial + " Endereço Final: " + enderecoFinal;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
