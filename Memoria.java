
import alocacaodememoria.GeradorAleatorio;
import alocacaodememoria.Processos;
import alocacaodememoria.SegmentoMemoria;
import alocacaodememoria.SegmentoVisual;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;


/**
 *
 * @author Bruno
 */

public class Memoria extends Thread {
    
    public static LinkedList memoria = new LinkedList();
//    public static LinkedList filaDeProcessos = new LinkedList();
    public static Processos[] processo = new Processos[200];
    public static GeradorAleatorio gerador = new GeradorAleatorio();
    public static long tempoExecucao;
    public static int qtdProcessos;
    public static int estrategiaAlocacao;
    public static int tamMemoria;
    public static SegmentoMemoria espacoLivreMemoria;
    public static ArrayList<SegmentoVisual> visualMemoria = new ArrayList();
    public static int tamSistemaOperacional;
    public static int inicioAreaOcupadaProcesso;
    public static int fimAreaOcupadaProcesso;
    public static int inicioTempoCriacaoProcesso;
    public static int fimTempoCriacaoProcesso;
    public static int inicioTempoDuracaoProcesso;
    public static int fimTempoDuracaoProcesso;
    public static int multMil;
    public static int divMil;
    /* Variáveis para os métodos de alocação */
    public static int endNextFit;
    public static boolean controleExecucao = false;
    
//    public static void main(String[] args){
//        
//        new InterfaceAlocacao().setVisible(true);       
//    }
     @Override
    public void run() {
        executa();
    }
    
    public static void executa(){
        tempoExecucao = InterfaceAlocacao.tempoExecucao;
        qtdProcessos = InterfaceAlocacao.qtdProcessos;
        estrategiaAlocacao = InterfaceAlocacao.estrategiaAlocacao;
        tamMemoria = InterfaceAlocacao.tamMemoria;
        espacoLivreMemoria = InterfaceAlocacao.espacoLivreMemoria;
        tamSistemaOperacional = InterfaceAlocacao.tamSistemaOperacional;
        inicioAreaOcupadaProcesso = InterfaceAlocacao.inicioAreaOcupadaProcesso;
        fimAreaOcupadaProcesso = InterfaceAlocacao.fimAreaOcupadaProcesso;
        inicioTempoCriacaoProcesso = InterfaceAlocacao.inicioTempoCriacaoProcesso;
        fimTempoCriacaoProcesso = InterfaceAlocacao.fimTempoCriacaoProcesso;
        inicioTempoDuracaoProcesso = InterfaceAlocacao.inicioTempoDuracaoProcesso;
        fimTempoDuracaoProcesso = InterfaceAlocacao.fimTempoDuracaoProcesso;
        multMil = 1000;
        divMil = 1000;        
//        System.out.println("Estratégia: " + estrategiaAlocacao);
        //Alocando sistema operacional
        for(int i = 0; i < tamSistemaOperacional; i++){
            memoria.add(i, "Sistema Operacional");
        }
        //Testes para os métodos Best e Worst fit
        for(int i = tamSistemaOperacional; i < tamMemoria; i++){
            memoria.add(i, " ");         
        }
        //Adicionando fim de memoria
        memoria.add(tamMemoria, "Fim de Memória");
        InterfaceAlocacao.mostrarMemoriaUtilizada();
        //Mostrar Memória
//        System.out.println("Endereço - Processo");
//        for(int i = 0; i < tamMemoria; i++){
//            System.out.println(i + " - " + memoria.get(i));
//        }
        visualMemoria.add(new SegmentoVisual("Sistema Operacional", tamSistemaOperacional, 0, tamSistemaOperacional - 1));
        visualMemoria.add(new SegmentoVisual("Livre", tamMemoria - tamSistemaOperacional, tamSistemaOperacional, tamMemoria - 1));
        InterfaceAlocacao.mostrarMemoria();
//        System.out.println("Tamanho da memória: " + memoria.size());
        //Criação dos processos
        //Primeiro processo
        int areaOcupada = gerador.gerarNumero(inicioAreaOcupadaProcesso, fimAreaOcupadaProcesso);
        int tempoDeCriacao = gerador.gerarNumero(inicioTempoCriacaoProcesso, fimTempoCriacaoProcesso);
        int tempoDeDuracao = gerador.gerarNumero(inicioTempoDuracaoProcesso, fimTempoDuracaoProcesso);

        processo[0] = new Processos(1, areaOcupada, tempoDeCriacao, tempoDeDuracao);
        InterfaceAlocacao.jTable2.setValueAt("P "+processo[0].getId(),0,0);
        InterfaceAlocacao.jTable2.setValueAt(""+processo[0].getAreaOcupada(),0,1);
        InterfaceAlocacao.jTable2.setValueAt(""+processo[0].getTempoDeDuracao(),0,2);
//        InterfaceAlocacao.jTable2.setValueAt(""+ processo[0].getTempoDeCriacao(),0,1);
        for(int i = 1; i < qtdProcessos; i++){
            int id = i + 1;
            areaOcupada = gerador.gerarNumero(inicioAreaOcupadaProcesso, fimAreaOcupadaProcesso);
            tempoDeCriacao = gerador.gerarNumero(inicioTempoCriacaoProcesso, fimTempoCriacaoProcesso);
            tempoDeDuracao = gerador.gerarNumero(inicioTempoDuracaoProcesso, fimTempoDuracaoProcesso);          
            
            processo[i] = new Processos(id, areaOcupada, (tempoDeCriacao + processo[i-1].getTempoDeCriacao()), tempoDeDuracao);
            InterfaceAlocacao.jTable2.setValueAt("P "+processo[i].getId(),i,0);
            InterfaceAlocacao.jTable2.setValueAt(""+processo[i].getAreaOcupada(),i,1);
            InterfaceAlocacao.jTable2.setValueAt(""+processo[i].getTempoDeDuracao(),i,2);
//            InterfaceAlocacao.jTable2.setValueAt(""+ processo[i].getTempoDeCriacao(),i,1);
        }   
        
        //Alocação e desalocação dos processos
        int auxAlocado = 0;
        int auxDesalocado = 0;
        int flag = 0;
        int naoPossivelAlocacao = 0, possivelAlocacao = 0;
        int numAlocados = 0, numDesalocados = 0;
        while(auxAlocado != qtdProcessos || auxDesalocado != qtdProcessos){
            
            for(int k = 0; k < qtdProcessos; k++){
                if(!processo[k].isCriado()){
                    if(processo[k].getTempoDeCriacao() * multMil < (System.currentTimeMillis() - tempoExecucao))  {
                        InterfaceAlocacao.jTable2.setValueAt(""+ processo[k].getTempoDeCriacao(),k,3);
                        processo[k].setCriado(true);
                    }                    
                }                              
            }
            for(int j = 0; j < qtdProcessos; j++){
                if(processo[j].getTempoDeCriacao() * multMil < (System.currentTimeMillis() - tempoExecucao)){
                    if(!processo[j].isFoiAlocado()){                        
                        alocarProcesso(processo[j], estrategiaAlocacao);
                        if(processo[j].isFoiAlocado()){
                            auxAlocado++;
//                            if(filaDeProcessos.contains(processo[j])){
//                            while(filaDeProcessos.contains(processo[j])){
//                                filaDeProcessos.remove(processo[j]);
//                            }
//                            InterfaceAlocacao.mostrarFilaDeProcessos();
//                            }
                            mostrarFilaDeProcessos();
                        }
                        else{
                            
                        //                            for(int i = 0; i < qtdProcessos; i++){
                            mostrarFilaDeProcessos();
//                            InterfaceAlocacao.jTable3.setValueAt("P "+ processo[j].getId(),j,0);
//                            InterfaceAlocacao.jTable3.setValueAt(""+ processo[j].getTempoDeCriacao(),j,1);                                
//                            }
                            
//                            if(!filaDeProcessos.contains(processo[j])){
//                                filaDeProcessos.add(processo[j]);
//                                InterfaceAlocacao.mostrarFilaDeProcessos();
//                            }
                        }   
                    }                    
                    else if(processo[j].isFoiAlocado() && !processo[j].isFoiDesalocado()){
                        if(processo[j].getTempoDeDuracao() <= ((System.currentTimeMillis() - processo[j].getTempoInicioExecucao()) / divMil)){
                            desalocarProcesso(processo[j]);
//                            mostrarFilaDeProcessos();
                            auxDesalocado++;                            
                        }
                    }
                } 
            }           
            if(((processo[qtdProcessos - 1].getTempoDeCriacao() * multMil) + (processo[qtdProcessos - 1].getTempoDeDuracao() * multMil)) < (System.currentTimeMillis() - tempoExecucao)){      
                for(int i = 0; i < qtdProcessos; i++){
                    if(processo[i].getContPossibilidade() != -1){
//                        System.out.println("Contador flag: " + flag);
                        flag++;
                    }
                }
                      
                if(flag <= qtdProcessos){
                    for(int i = 0; i < qtdProcessos; i++){
                        espacoLivreMemoria = qtdEspacoLivreMemoria(estrategiaAlocacao, processo[i].getAreaOcupada()); 
//                        System.out.println("Contador de possibilidade do processo "+processo[i].getId()+" é: "+processo[i].getContPossibilidade());
                        if(!processo[i].isFoiAlocado() && processo[i].getAreaOcupada() > espacoLivreMemoria.getEspaco()){                            
                            processo[i].setContPossibilidade(0);
                        }
                        else{
                            if(processo[i].getAreaOcupada() < espacoLivreMemoria.getEspaco()){
                                processo[i].setContPossibilidade(1);
                            }
                        }
                        if(processo[i].isFoiAlocado()){
                            processo[i].setContPossibilidade(1);
                        }
                    }                      
                }
                else{                    
                    for(int i = 0; i < qtdProcessos; i++){
                        espacoLivreMemoria = qtdEspacoLivreMemoria(estrategiaAlocacao, processo[i].getAreaOcupada()); 
                        if(processo[i].getContPossibilidade() == 1 && !processo[i].isConta()){
                            processo[i].setConta(true);  
                            possivelAlocacao++;                            
                        }
                        else if(processo[i].getContPossibilidade() == 0 && !processo[i].isConta()){
                            processo[i].setConta(true);
                            naoPossivelAlocacao++;
                        }
                    }
                    flag = 0;
//                    System.out.println("Possivel alocação: " + possivelAlocacao + " Não possível alocação: " + naoPossivelAlocacao);
                }
            }
            for(int i = 0; i < qtdProcessos; i++){
                if(processo[i].isFoiAlocado() && !processo[i].isContaAlocado()){
                    processo[i].setContaAlocado(true);
                    numAlocados++;
                }
                if(processo[i].isFoiDesalocado() && !processo[i].isContaDesalocado()){
                    processo[i].setContaDesalocado(true);
                    numDesalocados++;
                }
            }
            if((naoPossivelAlocacao + possivelAlocacao) == qtdProcessos && numAlocados == numDesalocados){
                for(int i = 0; i < qtdProcessos; i++){
                    espacoLivreMemoria = qtdEspacoLivreMemoria(estrategiaAlocacao, processo[i].getAreaOcupada()); 
                    if(processo[i].getAreaOcupada() > espacoLivreMemoria.getEspaco() && !processo[i].isFoiAlocado()){
                        //System.out.println("Não é possível alocar o processo " + processo[i].getId() + " (memória insuficiente).");
                        InterfaceAlocacao.jTable2.setValueAt("Sem Mem.",i,4);
                        InterfaceAlocacao.jTable2.setValueAt("Sem Mem.",i,5);
                        InterfaceAlocacao.jTable2.setValueAt("Sem Mem.",i,6);
                    }
                }
                auxAlocado = qtdProcessos;
                auxDesalocado = qtdProcessos;
            }            
        }

        //Visualizar as informações dos Processos
        double tempoDeEspera, soma = 0;
        for(int i = 0; i < qtdProcessos; i++){
            soma += processo[i].getTempoEspera();
        }
        tempoDeEspera = soma / qtdProcessos;
        DecimalFormat df = new DecimalFormat("#00.00");
        InterfaceAlocacao.lb_TempoMedioEspera.setText("" + df.format(tempoDeEspera));
        controleExecucao = true;
        memoria.clear();
    }
    
    public static void mostrarFilaDeProcessos(){
        int cont = 0, repetida = 0;
        String aux;
        
        for(int z = 0; z < Memoria.qtdProcessos; z++){
            for(int j = 0; j < Memoria.qtdProcessos; j++){
                aux = "P " + (processo[j].getId()+1);
                if(InterfaceAlocacao.jTable3.getValueAt(j, 0) == aux){
                    repetida++;
                }
            }
            if(processo[z].getTempoDeCriacao() * multMil < (System.currentTimeMillis() - Memoria.tempoExecucao) && !processo[z].isFoiAlocado() && repetida == 0){
                InterfaceAlocacao.jTable3.setValueAt("P "+ processo[z].getId(),cont,0);
                InterfaceAlocacao.jTable3.setValueAt(""+ processo[z].getTempoDeCriacao(),cont,1);    
                cont++;
            }   
            else{
                InterfaceAlocacao.jTable3.setValueAt("",cont,0);
                InterfaceAlocacao.jTable3.setValueAt("",cont,1);  
            }
        }
    }
    /* Retorna a quantidade de espaço livre na memória no momento que é chamado */
    public static SegmentoMemoria qtdEspacoLivreMemoria(int estrategiaAlocacao, int areaOcupada){
        int i, cont = 0, maior = 0, menor = 0;
        SegmentoMemoria espaco = new SegmentoMemoria();
        switch(estrategiaAlocacao){
            case 1:
                //First Fit - Pega o primeiro buraco que couber
                for(i = 0; i <= tamMemoria; i++){                    
                    if(memoria.get(i) == " "){                        
                        cont++;
                    }  
                    else{                        
                        if(cont >= areaOcupada){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            break;
                        }
                        cont = 0;
                    }
                }                               
                break;
            case 2:
                //Best Fit - Pega o menor buraco possível
                for(i = 0; i < tamMemoria; i++){
                    if(memoria.get(i) == " "){
                        cont++;                        
                    } 
                }
                menor = cont;
                for(i = 0, cont = 0; i <= tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;                        
                    }  
                    else{
                        if(cont <= menor && cont >= areaOcupada){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            menor = cont;
                        }
                        cont = 0;
                    }
                }  
                break;
            case 3:
                //Worst Fit - Pega o maior buraco possível
                for(i = 0; i <= tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                    }  
                    else{
                        if(cont > maior && cont >= areaOcupada){ 
                            maior = cont;
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);                            
                        }
                        cont = 0;
                    }
                }                
                break;
            case 4:
                //Next Fit
                for(i = 0; i <= tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                    }  
                    else{
                        if(cont >= areaOcupada){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            break;
                        }
                        cont = 0;
                    }
                }                               
                break;
        }        
        return espaco;  
    }

    public static void desalocarProcesso(Processos processo){
        //Verifica se o processo foi alocado
        if(processo.isFoiAlocado()){
//            System.out.println("Tempo finalizado do processo " + processo.getId() + " é: " + (System.currentTimeMillis() - tempoExecucao) / divMil);
            processo.setTempoFinalExecucao(System.currentTimeMillis());
            int i;
            int inicio = processo.getEndInicial();
            int fim = processo.getEndFinal();
            for(i = inicio; i < (fim + 1); i++){
                memoria.set(i, " ");                
            }
            processo.setFoiDesalocado(true);
//            System.out.println("Processo " + processo.getId() + " foi desalocado!");
//            System.out.println("Endereço - Processo");
//            for(i = 0; i < tamMemoria; i++){
//                System.out.println(i + " - " + memoria.get(i));
//            }
            int conclusao = (int) (processo.getTempoFinalExecucao() - tempoExecucao) / divMil;
            int espera = conclusao - processo.getTempoDeCriacao(); 
            processo.setTempoEspera(espera);
            double tempoDeEspera, soma = 0;
            for(int j = 0; j < processo.getId(); j++){
                soma += Memoria.processo[j].getTempoEspera();
            }
            tempoDeEspera = soma / processo.getId();
            DecimalFormat df = new DecimalFormat("#.00");
            InterfaceAlocacao.lb_TempoMedioEspera.setText("" + df.format(tempoDeEspera));
            InterfaceAlocacao.jTable2.setValueAt(""+ ((processo.getTempoFinalExecucao() - tempoExecucao) / divMil),processo.getId() - 1,5);           
            InterfaceAlocacao.jTable2.setValueAt(""+ (processo.getTempoEspera()),processo.getId() - 1,6);
            InterfaceAlocacao.mostrarMemoria();
            InterfaceAlocacao.mostrarMemoriaUtilizada();
        }
    }
    
    public static void alocarProcesso(Processos processo, int estrategiaAlocacao){
//        int i, contador, j, endereco;
        int i, cont = 0, maior = 0, menor = 0;
        SegmentoMemoria espaco = new SegmentoMemoria();
        switch(estrategiaAlocacao){
            /* Alocação First Fit - Pega o primeiro buraco que couber */
            case 1:                
                for(i = 0; i <= tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
//                        System.out.println("Contador dentro do if: " + cont);
                        cont++;                        
                    }  
                    else{
//                        System.out.println("Contador dentro do else: " + cont);
                        if(cont >= processo.getAreaOcupada()){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            break;
                        }
                        cont = 0;
                    }
                }  
                if(espaco.getEspaco() > 0){
                    processo.setEndInicial(espaco.getEnderecoInicial());
                    for(int j = espaco.getEnderecoInicial(); j < espaco.getEnderecoInicial() + processo.getAreaOcupada(); j++){
                        memoria.set(j, "Processo " + processo.getId());
                    }
                    processo.setEndFinal(espaco.getEnderecoInicial() + processo.getAreaOcupada() - 1);
                    processo.setFoiAlocado(true);
                    processo.setTempoInicioExecucao(System.currentTimeMillis());
//                    System.out.println("Tempo iniciado do alocação do processo " + processo.getId() + " é: " + (System.currentTimeMillis() - tempoExecucao)/divMil );
                }
                
                if(processo.isFoiAlocado()){
//                    System.out.println("Processo " + processo.getId() + " foi alocado!");
//                    System.out.println("Endereço - Processo");
//                    for(i = 0; i < tamMemoria; i++){
//                        System.out.println(i + " - " + memoria.get(i));
//                    }
//                    filaDeProcessos.remove(processo);
//                    InterfaceAlocacao.mostrarFilaDeProcessos();
                    InterfaceAlocacao.jTable2.setValueAt(""+ ((processo.getTempoInicioExecucao() - tempoExecucao) / divMil),processo.getId() - 1,4);
                    InterfaceAlocacao.mostrarMemoria();
                    InterfaceAlocacao.mostrarMemoriaUtilizada();
                }
                break;
            /* Alocação Best Fit - Pega o menor buraco possível */
            case 2:
                for(i = 0; i < tamMemoria; i++){
                    if(memoria.get(i) == " "){
                        cont++;
                    } 
                }
                menor = cont;
                for(i = 0, cont = 0; i <= tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                    }  
                    else{
                        if(cont <= menor && cont >= processo.getAreaOcupada()){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            menor = cont;
                        }
                        cont = 0;
                    }
                }
                if(espaco.getEspaco() > 0){
                    processo.setEndInicial(espaco.getEnderecoInicial());
                    for(int j = espaco.getEnderecoInicial(); j < espaco.getEnderecoInicial() + processo.getAreaOcupada(); j++){
                        memoria.set(j, "Processo " + processo.getId());
                    }
                    processo.setEndFinal(espaco.getEnderecoInicial() + processo.getAreaOcupada() - 1);
                    processo.setFoiAlocado(true);
                    processo.setTempoInicioExecucao(System.currentTimeMillis());
//                    System.out.println("Tempo iniciado do alocação do processo " + processo.getId() + " é: " + (System.currentTimeMillis() - tempoExecucao)/divMil );
                }
                
                if(processo.isFoiAlocado()){
//                    System.out.println("Processo " + processo.getId() + " foi alocado!");
//                    System.out.println("Endereço - Processo");
//                    for(i = 0; i < tamMemoria; i++){
//                        System.out.println(i + " - " + memoria.get(i));
//                    }
//                    InterfaceAlocacao.InterfaceAlocacao.mostrarMemoria();();
                    InterfaceAlocacao.jTable2.setValueAt(""+ ((processo.getTempoInicioExecucao() - tempoExecucao) / divMil),processo.getId() - 1,4);
                    InterfaceAlocacao.mostrarMemoria();
                    InterfaceAlocacao.mostrarMemoriaUtilizada();
                }
                break;                        
            /* Alocação Worst Fit - Pega o maior buraco possível */
            case 3:
                for(i = 0; i <= tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                    }  
                    else{
                        if(cont > maior && cont >= processo.getAreaOcupada()){ 
                            maior = cont;
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);                            
                        }
                        cont = 0;
                    }
                }    
                if(espaco.getEspaco() > 0){
                    processo.setEndInicial(espaco.getEnderecoInicial());
                    for(int j = espaco.getEnderecoInicial(); j < espaco.getEnderecoInicial() + processo.getAreaOcupada(); j++){
                        memoria.set(j, "Processo " + processo.getId());
                    }
                    processo.setEndFinal(espaco.getEnderecoInicial() + processo.getAreaOcupada() - 1);
                    processo.setFoiAlocado(true);
                    processo.setTempoInicioExecucao(System.currentTimeMillis());
//                    System.out.println("Tempo iniciado do alocação do processo " + processo.getId() + " é: " + (System.currentTimeMillis() - tempoExecucao)/divMil );
                }
                
                if(processo.isFoiAlocado()){
//                    System.out.println("Processo " + processo.getId() + " foi alocado!");
                    InterfaceAlocacao.mostrarMemoria();
                    InterfaceAlocacao.mostrarMemoriaUtilizada();
                    InterfaceAlocacao.jTable2.setValueAt(""+ ((processo.getTempoInicioExecucao() - tempoExecucao) / divMil),processo.getId() - 1,4);
//                    System.out.println("Endereço - Processo");
//                    for(i = 0; i < tamMemoria; i++){
//                        System.out.println(i + " - " + memoria.get(i));
//                    }
                }
                break;
            /* Alocação Next Fit*/
            case 4:
                if((tamMemoria - Memoria.endNextFit - 1) < inicioAreaOcupadaProcesso){
                    Memoria.endNextFit = tamSistemaOperacional;                    
                }
                for(i = Memoria.endNextFit; i <= tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                    }  
                    else{
                        if(cont >= processo.getAreaOcupada()){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            break;
                        }
                        cont = 0;
                    }
                }  
                if(espaco.getEspaco() > 0){
                    processo.setEndInicial(espaco.getEnderecoInicial());
                    for(int j = espaco.getEnderecoInicial(); j < espaco.getEnderecoInicial() + processo.getAreaOcupada(); j++){
                        memoria.set(j, "Processo " + processo.getId());
                    }
                    processo.setEndFinal(espaco.getEnderecoInicial() + processo.getAreaOcupada() - 1);
                    processo.setFoiAlocado(true);
                    processo.setTempoInicioExecucao(System.currentTimeMillis());
//                    System.out.println("Tempo iniciado do alocação do processo " + processo.getId() + " é: " + (System.currentTimeMillis() - tempoExecucao)/divMil );
                }
                
                if(processo.isFoiAlocado()){
                    Memoria.endNextFit = espaco.getEnderecoInicial() + processo.getAreaOcupada() - 1;
//                    System.out.println("Processo " + processo.getId() + " foi alocado!");
//                    System.out.println("Endereço - Processo");
//                    for(i = 0; i < tamMemoria; i++){
//                        System.out.println(i + " - " + memoria.get(i));
//                    }
                    InterfaceAlocacao.jTable2.setValueAt(""+ ((processo.getTempoInicioExecucao() - tempoExecucao) / divMil),processo.getId() - 1,4);
                    InterfaceAlocacao.mostrarMemoria();
                    InterfaceAlocacao.mostrarMemoriaUtilizada();
                }
            break;
        }        
    }
}
