import alocacaodememoria.SegmentoVisual;
import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bruno
 */
public class Teste {
    private static final LinkedList memoria = new LinkedList();
    private static SegmentoVisual espacoLivre = new SegmentoVisual();
    private static final int tamMemoria = 40;
    private static final int tamSistemaOperacional = 20;
    
    public static void main(String[] args){
        
        //Alocando sistema operacional
        for(int i = 0; i < tamSistemaOperacional; i++){
            memoria.add(i, "Sistema Operacional");
        }
        //Testes para os métodos Best e Worst fit
        for(int i = tamSistemaOperacional; i < 40; i++){
           
            memoria.add(i, " ");
            
        }
        //Mostrar Memória
        System.out.println("Endereço - Processo");
        for(int i = 0; i < tamMemoria; i++){
            System.out.println(i + " - " + memoria.get(i));
        }
        System.out.println("Tamanho da memória: " + memoria.size());
        int areaOcupada = 3;
        espacoLivre = memoriaVaga(2, areaOcupada);
        System.out.println(espacoLivre.toString());
    }
    
    public static SegmentoVisual memoriaVaga(int estrategiaAlocacao, int areaOcupada){
        int i, cont = 0, maior = 0, menor = 0;
        SegmentoVisual espaco = new SegmentoVisual();
        switch(estrategiaAlocacao){
            case 1:
                //First Fit - Pega o primeiro buraco que couber
                for(i = 0; i < tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                        System.out.println("Contador: " + cont);
                        if(cont == tamMemoria - tamSistemaOperacional && cont >= areaOcupada){
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(cont);
                            espaco.setEnderecoFinal(tamSistemaOperacional + cont - 1);   
                            break;
                        }
                    }  
                    else{
                        if(cont >= areaOcupada){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            break;
                        }
                        cont = 0;
                    }
                }                               
                break;
            case 2:
                //Best Fit - Pega o menor buraco possível
                for(i = 0; i < tamMemoria; i++){
                    if(memoria.get(i) == " "){
                        cont++;
                        if(cont == tamMemoria - tamSistemaOperacional && cont >= areaOcupada){
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(cont);
                            espaco.setEnderecoFinal(tamSistemaOperacional + cont - 1);   
                            break;
                        }
                    } 
                }
                menor = cont;
                for(i = 0, cont = 0; i < tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                    }  
                    else{
                        if(cont < menor && cont >= areaOcupada){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            menor = cont;
                        }
                        cont = 0;
                    }
                }  
                break;
            case 3:
                //Worst Fit - Pega o maior buraco possível
                for(i = 0; i < tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                        if(cont == tamMemoria - tamSistemaOperacional && cont >= areaOcupada){
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(cont);
                            espaco.setEnderecoFinal(tamSistemaOperacional + cont - 1);   
                            break;
                        }
                    }  
                    else{
                        if(cont > maior && cont >= areaOcupada){ 
                            maior = cont;
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);                            
                        }
                        cont = 0;
                    }
                }                
                break;
            case 4:
                //Next Fit
                for(i = 0; i < tamMemoria; i++){                    
                    if(memoria.get(i) == " "){
                        cont++;
                        if(cont == tamMemoria - tamSistemaOperacional && cont >= areaOcupada){
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(cont);
                            espaco.setEnderecoFinal(tamSistemaOperacional + cont - 1);   
                            break;
                        }
                    }  
                    else{
                        if(cont >= areaOcupada){ 
                            espaco.setEspaco(cont);
                            espaco.setEnderecoInicial(i - cont);
                            espaco.setEnderecoFinal(i - 1);   
                            break;
                        }
                        cont = 0;
                    }
                }                               
                break;
        }        
        return espaco;        
    }    
}