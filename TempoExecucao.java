/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gdeste
 */
public class TempoExecucao extends Thread {
    
    public long tempo;
    
    public void run(){
        tempo = System.currentTimeMillis();
        
        while(!Memoria.controleExecucao){             
            InterfaceAlocacao.lb_tempoAtual.setText(""+(System.currentTimeMillis() - tempo)/1000); 
        }
    }
    
}
